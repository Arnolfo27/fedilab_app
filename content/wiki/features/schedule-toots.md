*[Home](../../home) > [Features](../../features) &gt;* Schedule toots

<div align="center">
<img src="../../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## Schedule Toots

With Fedilab you can schedule toots. Here's how you can do it.

- [How to schedule a toot](#schedule-a-toot)
- [How to manage scheduled toots](#manage-scheduled-toots)

<br>

### Schedule a toot

1. Press new new toot button <br><br> <img src="../../res/schedule/schedule_1.png" width=50px>

2. Write a toot, like you usually do when tooting. <br><br> <img src="../../res/schedule/schedule_2.png" width=300px>

3. Open the three-dot-menu on top right corner and select 'Schedule' <br><br> <img src="../../res/schedule/schedule_3.png" width=300px>

4. Pick the date you want the toot to be tooted. Then press the 'next' icon. <br><br> <img src="../../res/schedule/schedule_4.png" width=300px>

5. And pick the time. And press 'done' icon on the bottom. <br><br> <img src="../../res/schedule/schedule_5.png" width=300px>

6. Then you have to choose a method to schedule your toot <br><br> <img src="../../res/schedule/schedule_6.png" width=300px>
   * **From device** <br> *schedule your toot using Fedilab's schedule feature. If you use this method your toot's content (text, media) will stay on your device. When the time comes Fedilab will toot them for you. However Fedilab needs* **a working internet connection** *for this to work.*

   * **From Server** <br> *schedule your toot using your instances' schedule feature. If you use this method your toot's content (text, media) will be uploaded to your instance. But they won't be published as a toot until the time your choose.*


7. Now you should see a toast message like this. <br><br> <img src="../../res/schedule/schedule_7.png" width=250px>

<br>

### Manage scheduled toots

1. In home screen, open the navigation drawer and select 'Scheduled toots' <br><br> <img src="../../res/schedule/manage_1.png" width=300px>

2. Here you can see your scheduled toots. <br><br> <img src="../../res/schedule/manage_2.png" width=300px><br>

 - You'll be able to see the time of the scheduled toots in blue color

 - Click on a toot to edit it

 - Use the blue cross button on the right to cancel the toot
