---
date: 2019-02-08
---
[![License: GPL v3](../res/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

<br>
<div align="center">
<img src="../res/fedilab_icon.png" width="70px">
<b>Fedilab</b><br><br>

Fedilab is a multi-accounts client for <br>
 [Mastodon](https://joinmastodon.org/)&nbsp;|&nbsp;[Peertube](https://joinpeertube.org/)&nbsp;|&nbsp;[Pixelfed](https://pixelfed.org/)&nbsp;| [Pleroma](https://pleroma.social/)&nbsp;|&nbsp;[GNU&nbsp;Social](https://gnu.io/social/)&nbsp;|&nbsp;[Friendica](https://friendi.ca/)
 </div>

The app will allow you to manage several accounts on Mastodon, Peertube and Pleroma instances. There are a lot of features and this wiki should help to know them and how to use them.

* [Features](../features)
* [Installation](../../install)
* [User Guide](../user-guide)
* [Tips](../tips)

Other apps:
* [UntrackMe](../untrackme)

<br>
<br>


**Developer of Fedilab:**
<br>&nbsp;&nbsp;&nbsp;&nbsp;Thomas - [toot.fedilab.app/@apps](https://toot.fedilab.app/@apps)

**Authors of the wiki:**
<br>&nbsp;&nbsp;&nbsp;&nbsp;Thomas - [toot.fedilab.app/@apps](https://toot.fedilab.app/@apps)<br>&nbsp;&nbsp;&nbsp;&nbsp;Kasun - [toot.fedilab.app/@kasun](https://toot.fedilab.app/@kasun)
